package services.curl.jwt.support;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static services.curl.jwt.config.BaseWebSecurityConfigurer.LOGIN_PATH;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import services.curl.jwt.authentication.RestAuthenticationFilter;
import services.curl.jwt.domain.JwtToken;

@Profile("!disableAuthenticationMock")
@Component
public class AuthenticationMock {

    @SuppressWarnings({"PMD.UnusedPrivateField", "PMD.SingularField"})
    @InjectMocks
    private final RestAuthenticationFilter restAuthenticationFilter;

    private final TestRestTemplate restTemplate;

    @Mock
    private AuthenticationManager authenticationManager;

    public AuthenticationMock(TestRestTemplate restTemplate, RestAuthenticationFilter restAuthenticationFilter) {
        this.restTemplate = restTemplate;
        this.restAuthenticationFilter = restAuthenticationFilter;
        MockitoAnnotations.initMocks(this);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        JacksonTester.initFields(this, objectMapper);
    }

    public HttpHeaders login(String principal, String... roles) {
        mockAuthentication(principal, roles);

        ResponseEntity<String> authResponse = attemptLogin();

        HttpHeaders headers = new HttpHeaders();
        headers.add("X-Auth", authResponse.getHeaders().get("X-Auth").get(0));
        return headers;
    }

    public void mockAuthentication(String principal, String... roles) {
        List<SimpleGrantedAuthority> grantedAuthorities =
                Stream.of(roles).map(SimpleGrantedAuthority::new).collect(Collectors.toList());
        JwtToken authentication = new JwtToken(grantedAuthorities, principal);
        authentication.setAuthenticated(true);

        when(authenticationManager.authenticate(any())).thenReturn(authentication);
    }

    @SuppressWarnings("unchecked")
    public void mockDisabledAccountAuthentication() {
        when(authenticationManager.authenticate(any())).thenThrow(DisabledException.class);
    }

    public ResponseEntity<String> attemptLogin() {
        return restTemplate.postForEntity(LOGIN_PATH,
                "{\"username\":\"any-user\", \"password\":\"any-password\"}", String.class);
    }
}
