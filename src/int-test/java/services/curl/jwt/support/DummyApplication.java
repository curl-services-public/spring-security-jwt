package services.curl.jwt.support;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Bootstraps the application.
 */
@SpringBootApplication(scanBasePackages = "services.curl.jwt")
@ComponentScan(basePackages = "services.curl.jwt")
public class DummyApplication {

}
