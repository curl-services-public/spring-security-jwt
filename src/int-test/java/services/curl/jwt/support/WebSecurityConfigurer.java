package services.curl.jwt.support;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import services.curl.jwt.SecurityApi;
import services.curl.jwt.config.BaseWebSecurityConfigurer;
import services.curl.jwt.config.SecurityConfig;

@Configuration
@EnableWebSecurity
public class WebSecurityConfigurer extends BaseWebSecurityConfigurer {

    private final String allowedOrigin;

    WebSecurityConfigurer(SecurityConfig securityConfig) {
        super(securityConfig);
        this.allowedOrigin = securityConfig.getCorsAllowedOrigin();
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception { // NOPMD, comes from the signature
        auth
                .inMemoryAuthentication()
                .withUser("test1").password("test").roles("USER");
    }

    @Override
    public void doProtectEndpoints(
            ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry httpAuthorize) {
        httpAuthorize.antMatchers(HttpMethod.GET, "/login-form.html").permitAll();
        httpAuthorize.antMatchers(HttpMethod.GET, "/**/*.css").permitAll();
        httpAuthorize.antMatchers(HttpMethod.GET, "/**/*.js").permitAll();
        httpAuthorize.antMatchers(HttpMethod.GET, "/index.html").permitAll();
        httpAuthorize.antMatchers(HttpMethod.GET, "/").permitAll();
        httpAuthorize.antMatchers(HttpMethod.GET, "/favicon.ico").permitAll();
        httpAuthorize.antMatchers(HttpMethod.OPTIONS, "**").permitAll();
        httpAuthorize.antMatchers(HttpMethod.POST, SecurityApi.PATH_RENEW_REFRESH_TOKEN).permitAll();
        httpAuthorize.antMatchers(HttpMethod.POST, SecurityApi.PATH_RENEW_ACCESS_TOKEN).permitAll();
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedMethods("*")
                        .allowedOrigins(allowedOrigin) // NOPMD - private modifier is just right
                        .allowedHeaders("*");
            }
        };
    }
}
