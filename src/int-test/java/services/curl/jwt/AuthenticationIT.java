package services.curl.jwt;

import java.io.IOException;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import services.curl.jwt.support.AuthenticationMock;
import services.curl.jwt.support.DummyApplication;

/**
 * <p> Integration test of authentication endpoint. </p>
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = DummyApplication.class)
@AutoConfigureJsonTesters
public class AuthenticationIT {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private AuthenticationMock authentication;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        MockitoAnnotations.initMocks(authentication);
    }

    @Test
    public void authenticationTest() throws IOException {
        // GIVEN
        authentication.mockAuthentication("test-principal", "ROLE_USER");
        // WHEN
        ResponseEntity<String> result = attemptLogin();
        // THEN
        Assert.assertEquals(HttpStatus.OK, result.getStatusCode());

        List<String> authHeader = result.getHeaders().get("X-Auth");
        Assert.assertNotNull(authHeader);
        Assert.assertFalse(authHeader.isEmpty());
        Assert.assertTrue(authHeader.get(0).startsWith("Bearer"));
    }

    @Test
    public void shoudFailIfAccountDisabled() throws IOException {
        // GIVEN
        authentication.mockDisabledAccountAuthentication();
        // WHEN
        ResponseEntity<String> result = attemptLogin();
        // THEN
        Assert.assertEquals(HttpStatus.UNAUTHORIZED, result.getStatusCode());
    }

    @Test
    public void authorizationTest() throws IOException {
        // GIVEN
        HttpHeaders headers = authentication.login("test-principal", "ACTUATOR");
        // WHEN
        ResponseEntity<String> result =
                restTemplate.exchange("/env", HttpMethod.GET, new HttpEntity<>(headers), String.class);
        // THEN
        Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
    }

    private ResponseEntity<String> attemptLogin() {
        return authentication.attemptLogin();
    }
}
