package services.curl.jwt;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.containsString;
import static services.curl.jwt.JwtSecurityContextRepository.TOKEN_PREFIX;
import static services.curl.jwt.config.BaseWebSecurityConfigurer.LOGIN_PATH;

import java.util.List;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import services.curl.jwt.support.DummyApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("disableAuthenticationMock")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = DummyApplication.class)
public class JwtSecurityIT {

    /**
     * Expiry: in 100yrs, secret: secret.
     */
    private static final String LONG_LIVED_ACCESS_TOKEN = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJleHAiOjQ2NTQwMDc0NTEsImF1dGh"
            + "vcml0aWVzIjpbeyJhdXRob3JpdHkiOiJ1c2VyIn1dLCJwcmluY2lwYWwiOnsidXNlcm5hbWUiOiJ0ZXN0MSIsImF1dGhvcml0aWVzIj"
            + "pbeyJhdXRob3JpdHkiOiJ1c2VyIn1dfSwicHJpbmNpcGFsVHlwZSI6ImNvbS5qaXZlc29mdHdhcmUudGFuZ28udXNlci5Vc2VyQWNjb"
            + "3VudCIsImF1dGhvcml0eVR5cGUiOiJvcmcuc3ByaW5nZnJhbWV3b3JrLnNlY3VyaXR5LmNvcmUuR3JhbnRlZEF1dGhvcml0eSJ9.VOv"
            + "Zp1yyG4GeVPn_IEzUOmMYOMj-P14Yl8TxsrZJblxcgiyTlBEyEelZHHnSowwNGXSLrSsa_XksdI_HtKskLw";
    /**
     * Expiry: in 100yrs, secret: secret, refresh_type: true.
     */
    private static final String LONG_LIVED_REFRESH_TOKEN = "eyJhbGciOiJIUzUxMiJ9.eyJleHAiOjQ2NTQwMDc0NTEsImF1dGhvcml0a"
            + "WVzIjpbeyJhdXRob3JpdHkiOiJ1c2VyIn1dLCJwcmluY2lwYWwiOnsidXNlcm5hbWUiOiJ0ZXN0MSIsImF1dGhvcml0aWVzIjpbeyJh"
            + "dXRob3JpdHkiOiJ1c2VyIn1dfSwicHJpbmNpcGFsVHlwZSI6ImNvbS5qaXZlc29mdHdhcmUudGFuZ28udXNlci5Vc2VyQWNjb3VudCI"
            + "sImF1dGhvcml0eVR5cGUiOiJvcmcuc3ByaW5nZnJhbWV3b3JrLnNlY3VyaXR5LmNvcmUuR3JhbnRlZEF1dGhvcml0eSIsInJlZnJlc2"
            + "hfdHlwZSI6InRydWUifQ.CoJT0_i9QVC8aS5HpYsqRb5KggbE2khw4G0DhhFwT8-miNDB28ufshgjXT9AfjWs-3a5jDUkPzinf5S29J"
            + "UFKQ";

    private final HttpHeaders httpHeaders = new HttpHeaders();
    private final RestTemplate rest = new RestTemplate();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @LocalServerPort
    private int port;

    private String baseUrl;

    @Before
    public void setUp() {
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.set("X-Requested-With", "XMLHttpRequest");
        baseUrl = "http://localhost:" + port;
    }

    @Test
    public void shouldGetCorsHeadersForOptionsOnLoginEndpoint() {
        ResponseEntity<String> response = rest.exchange(baseUrl + LOGIN_PATH, HttpMethod.OPTIONS,
                headers(), String.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        HttpHeaders headers = response.getHeaders();
        assertThat(headers).containsKey(HttpHeaders.ACCESS_CONTROL_ALLOW_METHODS);
        assertThat(headers).containsKey(HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS);
        assertThat(headers).containsKey(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS);
        assertThat(headers).containsKey(HttpHeaders.ACCESS_CONTROL_ALLOW_CREDENTIALS);
        assertThat(headers.get(HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS))
                .isNotEmpty()
                .isNotEqualTo("*");
    }

    //   @Test
    public void shouldGetNewRefreshToken() {
        ResponseEntity<String> response = postWithBody(baseUrl + LOGIN_PATH,
                "{\"username\":\"test1\",\"password\":\"test\"}");
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        List<String> values = response.getHeaders().get(JwtSecurityContextRepository.REFRESH_TOKEN_HEADER_KEY);
        assertThat(values).hasSize(1);
        String jwt = values.get(0);

        response = postWithBody(refreshTokenUrl(), jwt);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void shouldNotGetNewRefreshTokenForWrongCurrentToken() {
        expectBadRequest();
        postWithBody(refreshTokenUrl(), "wrong token");
    }

    @Test
    public void shouldNotGetNewRefreshTokenForAccessToken() {
        expectBadRequest();
        postWithBody(refreshTokenUrl(), LONG_LIVED_ACCESS_TOKEN.replace(TOKEN_PREFIX, ""));
    }

    @Test
    public void shouldGrantNewAccessTokenFromRefreshToken() {
        ResponseEntity<String> response = postWithBody(accessTokenUrl(), LONG_LIVED_REFRESH_TOKEN);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void shouldNotGrantNewAccessTokenFromExistingAccessToken() {
        expectBadRequest();
        postWithBody(accessTokenUrl(), LONG_LIVED_ACCESS_TOKEN.replace(TOKEN_PREFIX, ""));
    }

    private ResponseEntity<String> postWithBody(String url, String body) {
        return rest.exchange(url, HttpMethod.POST, body(body), String.class);
    }

    private String refreshTokenUrl() {
        return baseUrl + SecurityApi.PATH_RENEW_REFRESH_TOKEN;
    }

    private String accessTokenUrl() {
        return baseUrl + SecurityApi.PATH_RENEW_ACCESS_TOKEN;
    }

    private void expectBadRequest() {
        expectedException.expectMessage(containsString(HttpStatus.INTERNAL_SERVER_ERROR.value() + " null"));
    }

    private HttpEntity<String> headers() {
        return new HttpEntity<>(httpHeaders);
    }

    private HttpEntity<String> body(String body) {
        return new HttpEntity<>(body, httpHeaders);
    }
}
