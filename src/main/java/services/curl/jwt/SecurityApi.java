package services.curl.jwt;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public interface SecurityApi {

    String PATH_RENEW_REFRESH_TOKEN = "/auth/renewRefreshToken";
    String PATH_RENEW_ACCESS_TOKEN = "/auth/renewAccessToken";

    @PostMapping(path = PATH_RENEW_REFRESH_TOKEN, consumes = MediaType.APPLICATION_JSON_VALUE)
    String generateNewRefreshToken(@RequestBody String currentRefreshToken);

    @PostMapping(path = PATH_RENEW_ACCESS_TOKEN, consumes = MediaType.APPLICATION_JSON_VALUE)
    String generateNewAccessToken(@RequestBody String refreshToken);
}
