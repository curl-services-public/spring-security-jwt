package services.curl.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import services.curl.jwt.config.SecurityConfig;

/**
 * <p>
 * Responsible for serialization and deserialization of JWT tokens.
 * </p>
 */
public class JwtTokenParser {

    private static final SignatureAlgorithm SIGNATURE_ALGORITHM = SignatureAlgorithm.HS512;

    private final SecurityConfig securityConfig;

    public JwtTokenParser(SecurityConfig securityConfig) {
        this.securityConfig = securityConfig;
    }

    public Jws<Claims> readToken(String token) {
        try {
            return Jwts.parser().setSigningKey(securityConfig.getJwtTokenSecret()).parseClaimsJws(token);
        } catch (JwtException | IllegalArgumentException exception) {
            throw new SpringSecurityJwtException("Failed to parse token", exception);
        }
    }

    public String writeToken(JwtBuilder builder) {
        return builder.signWith(SIGNATURE_ALGORITHM, securityConfig.getJwtTokenSecret()).compact();
    }
}
