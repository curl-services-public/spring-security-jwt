package services.curl.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import java.util.Map;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.web.context.SecurityContextRepository;
import org.springframework.stereotype.Service;

@Service
public class JwtService {

    private final JwtSecurityContextRepository securityContextRepository;

    JwtService(SecurityContextRepository securityContextRepository) {
        this.securityContextRepository = (JwtSecurityContextRepository) securityContextRepository;
    }

    String generateNewRefreshToken(String currentRefreshToken) {
        Jws<Claims> refreshToken = validateRefreshToken(currentRefreshToken);
        String accessToken = convertRefreshTokenToAccessToken(refreshToken);
        SecurityContext securityContext = securityContextRepository.readSecurityContextFromToken(accessToken);
        return securityContextRepository.buildRefreshToken(securityContext);
    }

    String generateAccessTokenFromRefreshToken(String refreshToken) {
        Jws<Claims> token = validateRefreshToken(refreshToken);
        return convertRefreshTokenToAccessToken(token);
    }

    @SuppressWarnings("unchecked")
    private String convertRefreshTokenToAccessToken(Jws<Claims> currentRefreshToken) {
        JwtBuilder jwtBuilder = Jwts.builder();
        jwtBuilder.setHeader((Map) currentRefreshToken.getHeader());
        Map<String, Object> claims = currentRefreshToken.getBody();
        claims.remove(JwtSecurityContextRepository.REFRESH_TYPE_FIELD);
        jwtBuilder.setClaims(claims);
        return securityContextRepository.buildAccessToken(jwtBuilder);
    }

    private Jws<Claims> validateRefreshToken(String refreshToken) {
        Jws<Claims> tokenClaims = securityContextRepository.doReadToken(refreshToken);
        if (tokenClaims == null
                || !tokenClaims.getBody().containsKey(JwtSecurityContextRepository.REFRESH_TYPE_FIELD)) {
            throw new IllegalArgumentException("Invalid or expired refresh token");
        }
        return tokenClaims;
    }
}
