package services.curl.jwt;

public class SpringSecurityJwtException extends RuntimeException {

    public SpringSecurityJwtException(String message, Throwable cause) {
        super(message, cause);
    }
}
