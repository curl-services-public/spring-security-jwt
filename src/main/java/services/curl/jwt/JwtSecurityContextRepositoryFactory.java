package services.curl.jwt;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import lombok.experimental.UtilityClass;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.context.SecurityContextRepository;
import services.curl.jwt.config.SecurityConfig;
import services.curl.jwt.conversion.JwtSecurityContextTransformer;
import services.curl.jwt.conversion.SimpleGrantedAuthorityDeserializer;

/**
 * <p>
 * Utility class to simplify enabling Jwt based authentication and authorization integrated into Spring security by
 * providing factory methods for creating {@link SecurityContextRepository}. Example usage: <code>
 * org.springframework.security.config.annotation.web.builders.HttpSecurity http = ...
 * com.devfactory.security.jwt.domain.JwtSecurityContext = new JwtSecurityContext(...);
 * http.setSharedObject(SecurityContextRepository.class
 * , JwtSecurityContextRepositoryFactory.createJwtSecurityContextRepository(jwtSecurityContext));
 * </code>
 * </p>
 */
@UtilityClass
public class JwtSecurityContextRepositoryFactory {

    /**
     * Creates {@link SecurityContextRepository}.
     *
     * @param securityConfig context for JWT based security
     * @return newly created {@link SecurityContextRepository}
     */
    public SecurityContextRepository createJwtSecurityContextRepository(SecurityConfig securityConfig) {
        SimpleModule module = new SimpleModule();
        module.addDeserializer(GrantedAuthority.class, new SimpleGrantedAuthorityDeserializer());
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(module);
        return new JwtSecurityContextRepository(new JwtTokenParser(securityConfig),
                new JwtSecurityContextTransformer(securityConfig, objectMapper));
    }
}
