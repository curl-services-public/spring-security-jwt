package services.curl.jwt.authentication;

import static services.curl.jwt.config.BaseWebSecurityConfigurer.LOGIN_PATH;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import java.io.IOException;
import java.util.stream.Collectors;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.web.cors.CorsConfiguration;
import services.curl.jwt.JwtSecurityContextRepository;
import services.curl.jwt.domain.UserAccountCredentials;

/**
 * <p> Provides a username/password based authentication as part of Spring Security Filter Chain supporting REST calls.
 * </p>
 */
@Slf4j
public class RestAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private final ObjectMapper objectMapper;

    public RestAuthenticationFilter(String url, AuthenticationManager authenticationManager) {
        super(url);
        setAuthenticationManager(authenticationManager);
        setAuthenticationSuccessHandler(new NopAuthenticationSuccessHandler());
        objectMapper = new ObjectMapper();
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest httpServletRequest,
            HttpServletResponse httpServletResponse) throws IOException {
        // fix for OPTIONS being protected by authentication (which results in 500 response due to deserialization)
        if (httpServletRequest.getMethod().equals(HttpMethod.OPTIONS.toString())) {
            return null;
        }
        UserAccountCredentials credentials =
                objectMapper.readValue(httpServletRequest.getInputStream(), UserAccountCredentials.class);
        UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken(credentials.getUsername(), credentials.getPassword());
        Authentication authentication = null;
        try {
            authentication = getAuthenticationManager().authenticate(token);
        } catch (AuthenticationException exception) {
            log.info("Failed to authenticate user", exception);
            httpServletResponse.sendError(HttpStatus.UNAUTHORIZED.value());
        }
        if (authentication != null && authentication.isAuthenticated()) {
            log.info("Successful authentication of user {}. Groups: {}", authentication.getName(),
                    authentication.getAuthorities());
        }
        return authentication;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        String method = request.getMethod();
        if ((method.equals(HttpMethod.OPTIONS.toString()) || method.equals(HttpMethod.POST.toString()))
                && request.getRequestURI().equals(LOGIN_PATH)
                && !response.getHeaderNames().contains(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN)) {
            log.debug("applying custom CORS headers for OPTIONS due to being out of MVC filter chain");
            setCorsHeaders(request, response);
        }
        super.doFilter(req, res, chain);
    }

    private void setCorsHeaders(HttpServletRequest request, HttpServletResponse response) {
        ServerHttpResponse serverResponse = new ServletServerHttpResponse(response);
        CorsConfiguration permitAll = new CorsConfiguration().applyPermitDefaultValues();
        ImmutableList<String> headers = ImmutableList.of(HttpHeaders.CONTENT_TYPE,
                JwtSecurityContextRepository.AUTHORIZATION_HEADER_KEY,
                JwtSecurityContextRepository.REFRESH_TOKEN_HEADER_KEY,
                "X-Requested-With");
        permitAll.setAllowedHeaders(headers);
        permitAll.setExposedHeaders(headers);
        HttpHeaders responseHeaders = serverResponse.getHeaders();
        responseHeaders.setAccessControlAllowOrigin(request.getHeader(HttpHeaders.ORIGIN));
        responseHeaders.add(HttpHeaders.VARY, HttpHeaders.ORIGIN);
        responseHeaders.setAccessControlAllowMethods(permitAll.getAllowedMethods()
                .stream()
                .map(HttpMethod::valueOf)
                .collect(Collectors.toList()));
        responseHeaders.setAccessControlAllowHeaders(permitAll.getAllowedHeaders());
        responseHeaders.setAccessControlExposeHeaders(permitAll.getExposedHeaders());
        responseHeaders.setAccessControlAllowCredentials(true);
        responseHeaders.setAccessControlMaxAge(permitAll.getMaxAge());
        try {
            serverResponse.flush();
        } catch (IOException e) {
            log.error("Cannot set CORS headers", e);
        }
    }
}
