package services.curl.jwt.authentication;

import com.google.common.net.HttpHeaders;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.boot.autoconfigure.security.Http401AuthenticationEntryPoint;
import org.springframework.security.core.AuthenticationException;

/**
 * <p>
 * Provides an error handling entry point for HTTP 401 status with redirect option in case of non-ajax request.
 * </p>
 */
public class RestAuthenticationEntryPoint extends Http401AuthenticationEntryPoint {

    private final String redirectPath;

    public RestAuthenticationEntryPoint(String headerValue, String redirectPath) {
        super(headerValue);
        this.redirectPath = redirectPath;
    }

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response,
            AuthenticationException authException) throws IOException, ServletException {
        if ("XMLHttpRequest".equals(request.getHeader(HttpHeaders.X_REQUESTED_WITH))) {
            super.commence(request, response, authException);
        } else {
            response.sendRedirect(redirectPath);
        }
    }
}
