package services.curl.jwt.config;

import java.time.Duration;
import java.util.UUID;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Security configuration...
 */
@Component
@ConfigurationProperties(prefix = "app.web.security")
@Getter
@Setter
@Slf4j
public class SecurityConfig {


    /**
     * Time to live in minutes for an JWT access token. Default is 30 minutes.
     */
    private Long jwtAccessTokenTtlMinutes;

    /**
     * Time to live for an JWT access token. Default is 30 minutes. Specified as a ISO-8601 string. This value overrides
     * jwtAccessTokenTtlMinutes if both are specified.
     */
    private String jwtAccessTokenTtl;

    /**
     * Time to live in minutes for an JWT refresh token. Default is 60 minutes.
     */
    private Long jwtRefreshTokenTtlMinutes;

    /**
     * Time to live for an JWT refresh token. Default is 60 minutes. Specified as a ISO-8601 string. This value
     * overrides jwtRefreshTokenTtlMinutes if both are specified.
     */
    private String jwtRefreshTokenTtl;

    /**
     * JWT Token secret to use for JWT token signing. If no token secret is supplied, a random one will be used and
     * printed out to console during application startup.
     */
    private String jwtTokenSecret;

    /**
     * CORS allowed origin. If not defined, CORS headers will NOT be sent.
     */
    private String corsAllowedOrigin;

    @PostConstruct
    public void initialize() {
        if (jwtAccessTokenTtl != null) {
            Duration duration = Duration.parse(jwtAccessTokenTtl);
            jwtAccessTokenTtlMinutes = duration.toMinutes();
        }
        if (jwtRefreshTokenTtl != null) {
            Duration duration = Duration.parse(jwtRefreshTokenTtl);
            jwtRefreshTokenTtlMinutes = duration.toMinutes();
        }

        if (jwtAccessTokenTtlMinutes == null) {
            jwtAccessTokenTtlMinutes = 30L;
        }
        if (jwtRefreshTokenTtlMinutes == null) {
            jwtRefreshTokenTtlMinutes = 60L;
        }

        if (jwtTokenSecret == null) {
            jwtTokenSecret = UUID.randomUUID().toString();
            log.info("JWT Token Secret is not explicitly set. Secret set to: {}", jwtTokenSecret);
        }
    }

}
