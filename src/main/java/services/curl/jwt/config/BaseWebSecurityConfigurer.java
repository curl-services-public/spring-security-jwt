package services.curl.jwt.config;

import org.springframework.boot.autoconfigure.security.Http401AuthenticationEntryPoint;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.config.core.GrantedAuthorityDefaults;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.context.SecurityContextRepository;
import services.curl.jwt.JwtSecurityContextRepositoryFactory;
import services.curl.jwt.authentication.RestAuthenticationEntryPoint;
import services.curl.jwt.authentication.RestAuthenticationFilter;

@SuppressWarnings("PMD.SignatureDeclareThrowsException")
public abstract class BaseWebSecurityConfigurer extends WebSecurityConfigurerAdapter {

    public static final String LOGIN_PATH = "/auth/login";

    private final SecurityConfig securityConfig;

    public BaseWebSecurityConfigurer(SecurityConfig securityConfig) {
        this.securityConfig = securityConfig;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        enableJwt(http);
        ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry httpAuthorize =
                http.authorizeRequests();
        enableErrorHandling(http);
        protectEndpoints(httpAuthorize);
        enableJwtAuthentication(http);
    }

    @Bean
    public SecurityContextRepository createJwtSecurityContextRepository() {
        return JwtSecurityContextRepositoryFactory.createJwtSecurityContextRepository(securityConfig);
    }

    @Bean
    public GrantedAuthorityDefaults grantedAuthorityDefaults() {
        return new GrantedAuthorityDefaults("");
    }

    @Bean
    public RestAuthenticationFilter restAuthenticationFilter() throws Exception {
        return new RestAuthenticationFilter(LOGIN_PATH, authenticationManager());
    }

    @Bean
    public Http401AuthenticationEntryPoint restAuthenticationEntryPoint() {
        return new RestAuthenticationEntryPoint("Bearer ...", "/");
    }

    private void enableErrorHandling(HttpSecurity http) throws Exception {
        http.exceptionHandling().authenticationEntryPoint(restAuthenticationEntryPoint());
    }

    private void protectEndpoints(
            ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry httpAuthorize) {
        httpAuthorize.antMatchers(HttpMethod.POST, LOGIN_PATH).permitAll();
        doProtectEndpoints(httpAuthorize);
        httpAuthorize.anyRequest().authenticated();
    }

    public abstract void doProtectEndpoints(
            ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry httpAuthorize);

    private void enableJwtAuthentication(HttpSecurity http) throws Exception {
        http.addFilterBefore(restAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
    }

    private void enableJwt(HttpSecurity http) throws Exception {
        http.headers().cacheControl();
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.setSharedObject(SecurityContextRepository.class, createJwtSecurityContextRepository());
        http.csrf().disable();
    }

}
