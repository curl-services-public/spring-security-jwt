package services.curl.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtBuilder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.HttpRequestResponseHolder;
import org.springframework.security.web.context.SecurityContextRepository;
import services.curl.jwt.conversion.JwtSecurityContextTransformer;

/**
 * <p> Responsible to keep Spring's {@link SecurityContext} synchronized with the JWT token that travels as HTTP
 * metadata. </p>
 */
@Slf4j
public class JwtSecurityContextRepository implements SecurityContextRepository {

    public static final String REFRESH_TOKEN_HEADER_KEY = "Refresh-Token";
    public static final String REFRESH_TYPE_FIELD = "refresh_type";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String AUTHORIZATION_HEADER_KEY = "X-Auth";

    private final JwtTokenParser jwtTokenParser;
    private final JwtSecurityContextTransformer tokenTransformer;

    JwtSecurityContextRepository(JwtTokenParser jwtTokenParser, JwtSecurityContextTransformer tokenTransformer) {
        this.jwtTokenParser = jwtTokenParser;
        this.tokenTransformer = tokenTransformer;
    }

    @Override
    public SecurityContext loadContext(HttpRequestResponseHolder requestResponseHolder) {
        HttpServletRequest request = requestResponseHolder.getRequest();

        String token = acquireTokenFromRequest(request);
        SecurityContext context = null;
        if (token != null) {
            context = readSecurityContextFromToken(token);
            if (context != null) {
                refreshToken(context, requestResponseHolder.getResponse());
            }
        }

        if (context == null) {
            log.debug("No SecurityContext was available from header '{}'. Generating new context.",
                    AUTHORIZATION_HEADER_KEY);
            context = generateNewContext();
        }
        return context;
    }

    @Override
    public void saveContext(SecurityContext context, HttpServletRequest request, HttpServletResponse response) {
        if (!request.getMethod().equals(HttpMethod.OPTIONS.toString()) && context.getAuthentication() != null
                && !response.isCommitted()) {
            String token = writeSecurityContextIntoToken(context);
            addTokenAsHeader(response, token);
            addRefreshToken(context, response);
        }
    }

    @Override
    public boolean containsContext(HttpServletRequest request) {
        return request.getHeader(AUTHORIZATION_HEADER_KEY) != null;
    }

    private void refreshToken(SecurityContext context, HttpServletResponse response) {
        String token = writeSecurityContextIntoToken(context);
        addTokenAsHeader(response, token);
    }

    private String acquireTokenFromRequest(HttpServletRequest request) {
        String token = request.getHeader(AUTHORIZATION_HEADER_KEY);
        if (token == null) {
            log.debug("No JWT token currently exists behind header key {}", AUTHORIZATION_HEADER_KEY);
        }
        return token;
    }

    public String buildRefreshToken(SecurityContext context) {
        JwtBuilder jwtBuilder = tokenTransformer.convert(context)
                .claim(REFRESH_TYPE_FIELD, "true")
                .setExpiration(tokenTransformer.getRefreshTokenExpirationDate());
        return jwtTokenParser.writeToken(jwtBuilder);
    }

    private void addRefreshToken(SecurityContext context, HttpServletResponse response) {
        String token = buildRefreshToken(context);
        response.addHeader(REFRESH_TOKEN_HEADER_KEY, token);
    }

    private void addTokenAsHeader(HttpServletResponse response, String token) {
        response.addHeader(AUTHORIZATION_HEADER_KEY, TOKEN_PREFIX + token);
    }

    private SecurityContext generateNewContext() {
        return SecurityContextHolder.createEmptyContext();
    }

    private String writeSecurityContextIntoToken(SecurityContext context) {
        JwtBuilder jwtBuilder = tokenTransformer.convert(context);
        return jwtTokenParser.writeToken(jwtBuilder);
    }

    public SecurityContext readSecurityContextFromToken(String token) {
        SecurityContext result = null;
        Jws<Claims> parsedToken = doReadToken(token);
        if (parsedToken != null) {
            if (parsedToken.getBody().containsKey(REFRESH_TYPE_FIELD)) {
                log.warn("Cannot authenticate with refresh token, use access token");
                return null;
            }
            result = tokenTransformer.convert(parsedToken);
            log.debug("Obtained a valid SecurityContext from {}: '{}'", AUTHORIZATION_HEADER_KEY, result);
        } else {
            log.debug("Could not parse token: {}", token);
        }

        return result;
    }

    @SuppressWarnings("PMD.AvoidCatchingGenericException")
    public Jws<Claims> doReadToken(String token) {
        Jws<Claims> parsedToken;
        try {
            parsedToken = jwtTokenParser.readToken(token.replace(TOKEN_PREFIX, ""));
        } catch (Exception exception) {
            log.warn("Failed to read token", exception);
            parsedToken = null;
        }

        return parsedToken;
    }

    public String buildAccessToken(JwtBuilder token) {
        token.setExpiration(tokenTransformer.getAccessTokenExpirationDate());
        return jwtTokenParser.writeToken(token);
    }
}
