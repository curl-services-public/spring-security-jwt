package services.curl.jwt;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
class SecurityApiController implements SecurityApi {

    private final JwtService jwtService;

    SecurityApiController(JwtService jwtService) {
        this.jwtService = jwtService;
    }

    @Override
    public String generateNewRefreshToken(@RequestBody String currentRefreshToken) {
        return jwtService.generateNewRefreshToken(currentRefreshToken);
    }

    @Override
    public String generateNewAccessToken(@RequestBody String refreshToken) {
        return jwtService.generateAccessTokenFromRefreshToken(refreshToken);
    }
}
