package services.curl.jwt.conversion;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 * <p>
 * Forcibly deserializes {@link SimpleGrantedAuthority} instances for type {@link GrantedAuthority}. To be used with
 * Jackson Json Parser.
 * </p>
 */
public class SimpleGrantedAuthorityDeserializer extends StdDeserializer<GrantedAuthority> {

    private static final long serialVersionUID = 562136110481358224L;

    public SimpleGrantedAuthorityDeserializer() {
        super(GrantedAuthority.class);
    }

    @Override
    public GrantedAuthority deserialize(JsonParser parser, DeserializationContext ctxt) throws IOException {
        JsonNode node = parser.getCodec().readTree(parser);
        String role = node.get("authority").asText();
        return new SimpleGrantedAuthority(role);
    }
}
