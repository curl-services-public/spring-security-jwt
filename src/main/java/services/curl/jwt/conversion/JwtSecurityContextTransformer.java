package services.curl.jwt.conversion;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Collection;
import java.util.Date;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextImpl;
import services.curl.jwt.config.SecurityConfig;
import services.curl.jwt.domain.JwtToken;

/**
 * <p> Transforms between Spring's {@link SecurityContext} and {@link JwtBuilder} enabling JWT token based authorization
 * in Spring Security's context. </p>
 */
public class JwtSecurityContextTransformer {

    private static final String AUTHORITIES_KEY = "authorities";
    private static final String PRINCIPAL_KEY = "principal";
    private static final String DETAILS_KEY = "details";

    private static final String AUTHORITIES_TYPE_KEY = "authorityType";
    private static final String PRINCIPAL_TYPE_KEY = "principalType";
    private static final String DETAILS_TYPE_KEY = "detailsType";

    private final SecurityConfig securityConfig;
    private final ObjectMapper objectMapper;

    public JwtSecurityContextTransformer(SecurityConfig securityConfig, ObjectMapper objectMapper) {
        this.securityConfig = securityConfig;
        this.objectMapper = objectMapper;
    }

    /**
     * Converts {@link SecurityContext} to {@link JwtBuilder}.
     *
     * @param context holds authentication information
     * @return newly created {@link JwtBuilder} holding authentication information to be stored in a JWT token
     */
    public JwtBuilder convert(SecurityContext context) {
        Date expireTokenAt = getAccessTokenExpirationDate();

        JwtBuilder jwtBuilder = Jwts.builder();
        jwtBuilder.setExpiration(expireTokenAt);

        augmentAuthentication(jwtBuilder, context.getAuthentication());

        return jwtBuilder;
    }

    /**
     * Converts JWS (signed JWT) Claims to {@link SecurityContext}.
     *
     * @param source JWS claims
     * @return {@link SecurityContext} built based on the provided claims
     */
    public SecurityContext convert(Jws<Claims> source) {
        SecurityContextImpl result = new SecurityContextImpl();
        Authentication authentication = createAuthentication(source);
        result.setAuthentication(authentication);
        return result;
    }

    public Date getAccessTokenExpirationDate() {
        LocalDateTime tokenExpiry = LocalDateTime.now().plusMinutes(securityConfig.getJwtAccessTokenTtlMinutes());
        return Date.from(tokenExpiry.toInstant(ZoneOffset.UTC));
    }

    public Date getRefreshTokenExpirationDate() {
        LocalDateTime tokenExpiry = LocalDateTime.now().plusMinutes(securityConfig.getJwtRefreshTokenTtlMinutes());
        return Date.from(tokenExpiry.toInstant(ZoneOffset.UTC));
    }

    private Authentication createAuthentication(Jws<Claims> source) {
        Claims claims = source.getBody();

        JavaType authorityCollectionType =
                objectMapper.getTypeFactory().constructCollectionType(Collection.class, GrantedAuthority.class);

        Collection<? extends GrantedAuthority> authorities =
                objectMapper.convertValue(claims.get(AUTHORITIES_KEY), authorityCollectionType);

        Object principal = acquireClaim(claims, PRINCIPAL_KEY, PRINCIPAL_TYPE_KEY);

        JwtToken result = new JwtToken(authorities, principal);

        result.setDetails(acquireClaim(claims, DETAILS_KEY, DETAILS_TYPE_KEY));
        result.setAuthenticated(Boolean.TRUE);
        return result;
    }

    private Object acquireClaim(Claims claims, String claimKey, String claimTypeKey) {
        Object claim = claims.get(claimKey);
        Object result = null;
        if (claim != null) {
            Class<?> claimClass = acquireClaimClass(claims, claimTypeKey);
            result = objectMapper.convertValue(claim, claimClass);
        }
        return result;
    }

    private Class<?> acquireClaimClass(Claims claims, String claimTypeKey) {
        String claimClassName = claims.get(claimTypeKey, String.class);
        Class<?> claimClass;
        try {
            claimClass = Class.forName(claimClassName);
        } catch (ClassNotFoundException exception) {
            throw new IllegalArgumentException(
                    String.format("Couldn't find class '%s' for claimTypeKey '%s'", claimClassName, claimTypeKey),
                    exception);
        }
        return claimClass;
    }

    private void augmentAuthentication(JwtBuilder builder, Authentication authentication) {
        builder.claim(AUTHORITIES_KEY, authentication.getAuthorities());
        builder.claim(PRINCIPAL_KEY, authentication.getPrincipal());
        builder.claim(DETAILS_KEY, authentication.getDetails());

        setClaimClassSafely(builder, PRINCIPAL_TYPE_KEY, authentication.getPrincipal());
        setClaimClassSafely(builder, DETAILS_TYPE_KEY, authentication.getDetails());

        if (authentication.getAuthorities() != null && authentication.getAuthorities().size() > 0) {
            builder.claim(AUTHORITIES_TYPE_KEY, GrantedAuthority.class);
        }
    }

    private void setClaimClassSafely(JwtBuilder builder, String claimKey, Object claim) {
        if (claim != null) {
            builder.claim(claimKey, claim.getClass());
        }
    }
}
