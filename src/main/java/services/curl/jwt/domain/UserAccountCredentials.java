package services.curl.jwt.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * <p> Represents credentials to be used to authenticate a client. </p>
 */
@Getter
@Setter
public class UserAccountCredentials {

    private String username;
    private String password;

}
