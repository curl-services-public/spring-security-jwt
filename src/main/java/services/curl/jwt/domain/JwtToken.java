package services.curl.jwt.domain;

import java.util.Collection;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

/**
 * Authentication token for JWT-based security.
 */
public class JwtToken extends AbstractAuthenticationToken {

    private static final long serialVersionUID = 2017213599058903392L;

    private final Object principal;

    public JwtToken(Collection<? extends GrantedAuthority> authorities, Object principal) {
        super(authorities);
        this.principal = principal;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return principal;
    }

}
