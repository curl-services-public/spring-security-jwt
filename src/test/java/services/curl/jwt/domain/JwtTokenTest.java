package services.curl.jwt.domain;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

public class JwtTokenTest {

    @Test
    public void testGetCredentialsReturnsNull() throws Exception {
        JwtToken underTest = new JwtToken(singletonList(new SimpleGrantedAuthority("role")), null);
        assertThat(underTest.getCredentials()).isNull();
    }

}
