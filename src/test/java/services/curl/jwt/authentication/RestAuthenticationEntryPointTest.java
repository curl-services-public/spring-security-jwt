package services.curl.jwt.authentication;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.google.common.net.HttpHeaders;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.authentication.BadCredentialsException;

@RunWith(MockitoJUnitRunner.class)
public class RestAuthenticationEntryPointTest {

    @Mock
    HttpServletRequest httpServletRequest;

    @Mock
    HttpServletResponse httpServletResponse;


    @Test
    public void testCommenceWhenNotAjaxCall() throws Exception {
        RestAuthenticationEntryPoint undertTest = new RestAuthenticationEntryPoint("Bearer ...", "/");

        undertTest.commence(httpServletRequest, httpServletResponse, new BadCredentialsException(""));
        verify(httpServletResponse, times(1)).sendRedirect(eq("/"));
    }

    @Test
    public void testCommenceWhenAjaxCall() throws Exception {
        RestAuthenticationEntryPoint undertTest = new RestAuthenticationEntryPoint("Bearer ...", "/");
        when(httpServletRequest.getHeader(eq(HttpHeaders.X_REQUESTED_WITH))).thenReturn("XMLHttpRequest");

        undertTest.commence(httpServletRequest, httpServletResponse, new BadCredentialsException(""));
        verify(httpServletResponse, never()).sendRedirect(eq("/"));
    }

}
