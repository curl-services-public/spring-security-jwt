package services.curl.jwt.conversion;

import static org.junit.Assert.assertEquals;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import services.curl.jwt.config.SecurityConfig;

@RunWith(MockitoJUnitRunner.class)
public class JwtSecurityContextTransformerTest {

    private static final Long JWT_TOKEN_TTL = 12L;
    private static final String AUTHORITIES_KEY = "authorities";
    private static final String PRINCIPAL_KEY = "principal";
    private static final String DETAILS_KEY = "details";
    private static final String PRINCIPAL_TYPE_KEY = "principalType";
    private static final String DETAILS_TYPE_KEY = "detailsType";
    private static final String TEST_ROLE_USER = "test-user";
    private static final String TEST_ROLE_ADMIN = "test-admin";

    @InjectMocks
    private JwtSecurityContextTransformer underTest;

    @Mock
    private Jws<Claims> jwsClaims;

    @Mock
    private Claims claims;

    @Mock
    @SuppressWarnings("PMD.SingularField")
    private StdDeserializer<GrantedAuthority> authorityDeserializer;

    @Before
    public void setUp() throws IOException {
        SimpleModule module = new SimpleModule();
        // technical debt: deserializer should be mocked
        authorityDeserializer = new SimpleGrantedAuthorityDeserializer();
        module.addDeserializer(GrantedAuthority.class, authorityDeserializer);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(module);
        SecurityConfig baseSecurityConfig = new SecurityConfig();
        baseSecurityConfig.setJwtAccessTokenTtlMinutes(JWT_TOKEN_TTL);
        baseSecurityConfig.setJwtRefreshTokenTtlMinutes(JWT_TOKEN_TTL);
        underTest = new JwtSecurityContextTransformer(baseSecurityConfig, objectMapper);
    }

    @Test
    public void testConvertFromClaimsShouldReturnSecurityContextSetUp() throws Exception {
        // GIVEN
        BDDMockito.given(jwsClaims.getBody()).willReturn(claims);
        Collection<? extends GrantedAuthority> authorities = createAuthorities();

        String principal = "principal";
        String details = "details";

        BDDMockito.given(claims.get(AUTHORITIES_KEY)).willReturn(authorities);
        BDDMockito.given(claims.get(PRINCIPAL_KEY)).willReturn(principal);
        BDDMockito.given(claims.get(DETAILS_KEY)).willReturn(details);

        BDDMockito.given(claims.get(PRINCIPAL_TYPE_KEY, String.class)).willReturn(Object.class.getCanonicalName());
        BDDMockito.given(claims.get(DETAILS_TYPE_KEY, String.class)).willReturn(Object.class.getCanonicalName());

        // WHEN
        SecurityContext result = underTest.convert(jwsClaims);
        // THEN
        assertEquals(authorities, result.getAuthentication().getAuthorities());
        assertEquals(principal, result.getAuthentication().getPrincipal());
        assertEquals(details, result.getAuthentication().getDetails());
    }

    private Collection<? extends GrantedAuthority> createAuthorities() {
        return Arrays.asList(new SimpleGrantedAuthority(TEST_ROLE_USER), new SimpleGrantedAuthority(TEST_ROLE_ADMIN));
    }
}
