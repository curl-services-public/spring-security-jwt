package services.curl.jwt;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.impl.DefaultJwsHeader;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class JwtServiceTest {

    private static final String TOKEN1 = "old";

    @InjectMocks
    private JwtService sut;

    @Mock
    private JwtSecurityContextRepository mockContextRepository;

    @Mock
    private Jws<Claims> mockJws;

    @Mock
    private Claims mockClaims;

    @Test
    public void shouldGenerateNewRefreshToken() {
        when(mockJws.getBody()).thenReturn(mockClaims);
        when(mockJws.getHeader()).thenReturn(new DefaultJwsHeader());
        when(mockClaims.containsKey("refresh_type")).thenReturn(true);
        when(mockContextRepository.doReadToken(TOKEN1)).thenReturn(mockJws);
        when(mockContextRepository.buildRefreshToken(any())).thenReturn("new");
        String refreshToken = sut.generateNewRefreshToken(TOKEN1);
        assertThat(refreshToken).isEqualTo("new");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAllowAccessToken() {
        prepareNonRefreshToken();
        sut.generateNewRefreshToken(TOKEN1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAllowForInvalidToken() {
        prepareWrongToken();
        sut.generateNewRefreshToken(TOKEN1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotGrantNewAccessTokenForInvalidRefreshToken() {
        prepareWrongToken();
        sut.generateAccessTokenFromRefreshToken(TOKEN1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotGrantNewAccessTokenForNonRefreshToken() {
        prepareNonRefreshToken();
        sut.generateAccessTokenFromRefreshToken(TOKEN1);
    }

    private void prepareWrongToken() {
        when(mockContextRepository.doReadToken(TOKEN1)).thenReturn(null);
    }

    private void prepareNonRefreshToken() {
        when(mockJws.getBody()).thenReturn(mockClaims);
        when(mockClaims.containsKey("refresh_type")).thenReturn(false);
        when(mockContextRepository.doReadToken(TOKEN1)).thenReturn(mockJws);
    }
}
