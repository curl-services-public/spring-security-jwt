package services.curl.jwt.config;

import static org.assertj.core.api.Assertions.assertThat;
import static pl.pojo.tester.api.assertion.Assertions.assertPojoMethodsFor;

import org.junit.Test;
import pl.pojo.tester.api.assertion.Method;

public class SecurityConfigTest {

    @Test
    public void testGetterSetter() {
        assertPojoMethodsFor(SecurityConfig.class).testing(Method.GETTER, Method.SETTER).areWellImplemented();
    }

    @Test
    public void testInitialize() throws Exception {
        SecurityConfig underTest = new SecurityConfig();
        underTest.setJwtRefreshTokenTtlMinutes(13L);
        underTest.setJwtAccessTokenTtlMinutes(26L);
        underTest.setJwtTokenSecret("secret");

        underTest.initialize();

        assertThat(underTest.getCorsAllowedOrigin()).isNull();
        assertThat(underTest.getJwtAccessTokenTtl()).isNull();
        assertThat(underTest.getJwtAccessTokenTtlMinutes()).isEqualTo(26L);
        assertThat(underTest.getJwtRefreshTokenTtl()).isNull();
        assertThat(underTest.getJwtRefreshTokenTtlMinutes()).isEqualTo(13L);
        assertThat(underTest.getJwtTokenSecret()).isEqualTo("secret");
    }

    @Test
    public void testInitialize2() throws Exception {
        SecurityConfig underTest = new SecurityConfig();
        underTest.setJwtAccessTokenTtl("PT33M");
        underTest.setJwtRefreshTokenTtl("PT66M");

        underTest.initialize();

        assertThat(underTest.getCorsAllowedOrigin()).isNull();
        assertThat(underTest.getJwtAccessTokenTtlMinutes()).isEqualTo(33L);
        assertThat(underTest.getJwtRefreshTokenTtlMinutes()).isEqualTo(66L);
    }
}
